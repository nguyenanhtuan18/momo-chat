# React Native Momo Chat SDK

## Install
```sh
yarn add momo-chat
```
## Peer Dependencies

```sh
yarn add @momo-platform/api @momo-kits/core @momo-platform/utils
```

## Create new Momo Feature

### Example:
- Code: 
```json
feature_chat_conversation
```
- Default Config: 
```json
{"screen":"MomoChatScreen", "barStyle":"light-content"}
```
- Feature Parameter: 
```json
{"isStartFromFeatureCode":true}
```

- Mini App: 
```json
vn.momo.transfer
```
## Usage

```js
import { MomoChatScreen } from 'momo-chat'

// Add MomoChatScreen to SCREENS constant
const SCREENS = {
    ...,
    MomoChatScreen
}

//Open Chat Screen
MaxApi.startFeatureCode('feature_chat_conversation', {
    animationType: 'Stack',
    friendId: '0962023623',
    friendName: 'Lê Thị Thuý',
    roomId: "9223370391964090893a7a21a6b-5013-4272-88bb-233a916c7316"
})
```

