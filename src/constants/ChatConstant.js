import { Spacing } from '@momo-kits/core';
import ChatLocalization from '../localizations';
import ChatImages from '../assets';

const ChatStatus = {
    Strange: 'Strange',
    Read: 'Read',
    UnRead: 'UnRead',
};
/**
 * @name getFontWeightChatTypeString
 * @param {string} type
 * @description return fontweight string to apply style component. It base on type.
 */
const getFontWeightChatTypeString = type => {
    if (type === ChatStatus.UnRead) {
        return 'bold';
    }
    return 'normal';
};

export default {
    ChatApiPath: {
        V1: {
            getUserInfo: 'helios/chat-api/v1/user',
            fetchRooms: 'helios/chat-api/v1/room/load',
            fetchPendingRooms: 'helios/chat-api/v1/room/load/pending',
            fetchRoomInfo: 'helios/chat-api/v1/room/detail/',
            fetchAllFriends: 'helios/chat-api/v1/user/friends/list',
            fetchMessageRoom: 'helios/chat-api/v1/room/fetch-messages',
            friendRequest: 'helios/chat-api/v1/user/send-friend-request',
            blockFriendRequest: 'helios/chat-api/v1/user/block',
            acceptFriendRequest: 'helios/chat-api/v1/user/accept-friend-request',
            unblockFriendRequest: 'helios/chat-api/v1/user/unblock',
            friendRelationShipStatus: 'helios/chat-api/v1/user/relationship',
            friendRequestPending: 'helios/chat-api/v1/user/friends/request/me/list',
            hideRoom: 'helios/chat-api/v1/room/hide',
            unhideRoom: 'helios/chat-api/v1/room/unhide',
            muteRoom: 'helios/chat-api/v1/room/mute',
            unMuteRoom: 'helios/chat-api/v1/room/unmute',
            getChatConfigs: 'helios/chat-api/v1/admin/config/system/common-config',
            removeUserFromRoom: 'helios/chat-api/v1/room/remove-user',
            searchRoom: 'helios/chat-api/v1/room/search/rooms',
            getActiveStatus: 'helios/chat-api/v1/user/friend/online/status',
            deleteMessagesByRoomId: 'helios/chat-api/v1/room/clear/messages',
            reportRoomByPhone: 'helios/chat-api/v1/user/report',
            getUserConfigs: 'helios/chat-api/v1/user/get/user-config',
            setUserConfigs: 'helios/chat-api/v1/user/set/user-config',
            pinRoom: 'helios/chat-api/v1/room/pin',
            unpinRoom: 'helios/chat-api/v1/room/unpin',
            pinMessages: 'helios/chat-api/v1/room/pin-messages',
            getPinMessages: 'helios/chat-api/v1/room/load-pin-messages',
            changeOrderPinMessages: 'helios/chat-api/v1/room/change-order-pinned-messages',
            shortShareLink: 'helios/chat-api/v1/room/create/universal-link',
            sendMessagePreview: 'helios/chat-api/v1/room/send/message',
            checkShortLinkExpired: 'helios/chat-api/v1/room/universal-link/status',
            addToGroupV2: 'helios/chat-api/v1/room/join/room',
            getRoomInfoStranger: 'helios/chat-api/v1/room/get/room/stranger',
            getTrendingGiphy: 'https://api.giphy.com/v1/gifs/trending',
            disableInviteLink: 'helios/chat-api/v1/room/universal-link/disable',
            searchGiphy: 'https://api.giphy.com/v1/gifs/search',
        },
    },
    ChatSchema: {
        FriendSchema: 'ChatFriendSchema',
        RoomSchema: 'ChatRoomSchema',
        MessageSchema: 'ChatRoomMessageSchema',
        ChatMessageIndexSchema: 'ChatMessageIndexSchema',
        ChatMessagePayloadSchema: 'ChatMessagePayloadSchema',
    },
    Relationship: {
        Friend: 0,
        Blocked: 1,
        Waiting: 2,
        Declined: 3,
        None: 4,
    },
    ChatStatus,
    getFontWeightChatTypeString,
};

export const ListActionSeen = [
    {
        key: 'view_profile',
        title: ChatLocalization.viewProfile,
        icon: '24_basic_account',
    },
    {
        key: 'chat_to_user',
        title: ChatLocalization.chat,
        icon: '24_chatting_comment_empty',
    },
];

export const STATUS_MESSAGE = {
    NOTSENT: 'NOTSENT',
    SENDING: 'SENDING',
    SENT: 'SENT',
    RECEIVED: 'RECEIVED',
    READ: 'READ',
};

export const EnumGRPCType = {
    MESSAGE_TEMP: 'MESSAGE_TEMP',
    MESSAGE_SENT: 'MESSAGE_SENT',
    NEW_MESSAGE: 'NEW_MESSAGE',
    UPDATE_MESSAGE: 'UPDATE_MESSAGE',
    UPDATE_BADGE_ICON: 'UPDATE_BADGE_ICON',
    ADD_PIN: 'ADD_PIN',
    REMOVE_PIN: 'REMOVE_PIN',
    READ_CURSOR: 'READ_CURSOR',
    RECEIVED: 'RECEIVED',
    UPDATE_SHORT_LINK: 'UPDATE_SHORT_LINK',
};

export const TRANSPARENT = 'transparent';

export const STATUS = {
    SUCCESS: 1,
    PENDING: 2,
    DENY: 3,
    CANCEL: 4,
    PAID: 5,
    HIDE: 6,
    CONFIRM: 7,
    REJECT: 8,
};

export const LIMIT_MONEY = {
    MIN: 1000,
    MAX: 20000000,
};

export const STATUS_VALIDATE = {
    NO_USER: -1,
    FAIL: 0,
    SUCCESS: 1,
};

export const TYPE_SCREEN = {
    SEEN_LIST: 'SEEN_LIST',
};

export const MESSAGE_TYPE = {
    TRANSFER: 'Transfer',
    REQUEST_MONEY: 'Request',
    MONEY_GIFT: 'MONEY_GIFT',
};

export const OPTION_MENU_TYPE = {
    GOTIT: 'GOTIT',
    TRANSFER: 'TRANSFER',
    LOTTE: 'LOTTE',
    TOPUP: 'TOPUP',
    GIFT_MONEY: 'GIFT_MONEY',
    REQUEST_MONEY: 'REQUEST_MONEY',
    GreetingCard: 'GreetingCard',
    RequestLuckyMoney: 'RequestLuckyMoney',
    TO_P2P_WOMEN_DAY: 'TO_P2P_WOMEN_DAY',
};

export const ACTION_KOL_TYPE = {
    FOLLOW: 'follow',
    UNFOLLOW: 'unfollow',
    REPORT: 'report',
};

/**
 * Enum for recommend spacing in chat mini-app
 * @readonly
 * @enum {number}
 */
export const CHAT_RECOMMEND_SPACING = {
    /** Right padding for bubble if target is current user */
    BUBBLE_RIGHT_PADDING: Spacing.XL,
};

export const CHAT_OBSERVER_KEY = {
    CHAT_ROOM_ACTIVE: 'CHAT_ROOM_ACTIVE',
};

export const DATE_TIME_FORMAT = {
    TIME_MESSAGE: 'HH:mm',
    VI: 'DD [thg] MM*, HH:mm',
    OTHER: 'MMM DD*, HH:mm',
    DATE_LAST_MESSAGE_VI: 'DD [thg] MM',
    DATE_LAST_MESSAGE_OTHER: 'MMM DD',
    DATE_LAST_MESSAGE: 'DD/MM',
    DATE_LAST_MESSAGE_WITH_YEAR: 'DD/MM/YYYY',
};

export const CHAT_ROOM_TYPE = {
    PENDING_ROOM: 'PENDING_ROOM',
    REQUEST_ROOM: 'REQUEST_ROOM',
    NORMAL_ROOM: 'NORMAL_ROOM',
    SPAM_ROOM: 'SPAM_ROOM',
};

export const DELAY_TIME = {
    longPress: 120,
};

export const PAGE_NEWS_FEED_ID = [
    107054575906874,
    107054184414924,
    107063984861813,
    107041849753028,
    107064898104565,
    107054355990591,
    107054566841665,
    107054546463728,
    107054559964536,
    107054550061041,
    107054542323882,
    107054369709322,
    107054562186776,
    107161161104660,
    107054554461141,
    107059573304974,
];

export const configShortLink = {
    action: 'marketing',
    serviceCode: 'join_group_with_link',
};

export const regexUrl = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/i;

export const ImagesReaction = [
    {
        reaction: ':love:',
        image: ChatImages.reaction_love,
        gif: ChatImages.pro_reaction_love_gif,
    },
    {
        reaction: ':haha:',
        image: ChatImages.reaction_happy,
        gif: ChatImages.pro_reaction_haha_gif,
    },
    {
        reaction: ':curious:',
        image: ChatImages.reaction_curious,
        gif: ChatImages.pro_reaction_wow_gif,
    },
    {
        reaction: ':sad:',
        image: ChatImages.reaction_sad,
        gif: ChatImages.pro_reaction_sad_gif,
    },
    {
        reaction: ':angry:',
        image: ChatImages.reaction_angry,
        gif: ChatImages.pro_reaction_angry_gif,
    },
];
