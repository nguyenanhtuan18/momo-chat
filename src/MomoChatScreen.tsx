import React, { useEffect, useState, useCallback, useRef} from 'react'
import { StyleSheet, SafeAreaView, ActivityIndicator } from 'react-native'
import MaxApi from '@momo-platform/api'
import get from 'lodash/get'
import findIndex from 'lodash/findIndex'
import { formatPhone, generateRequestId } from './helpers/ChatHelper'
import ChatService from './services/ChatService'
import { GiftedChat, IMessage } from 'react-native-gifted-chat'
import { useIsMountedRef } from './hooks/useIsMountedRef'
import { useIsInitialized } from './hooks/useIsInitialized'
import { checkDuplicateMessageFlag, parseMessageDataFromServer } from './services/ChatFunctions'
import { EnumGRPCType } from './types'
import { STATUS_MESSAGE } from './constants/ChatConstant'

/**
 * 1. Get Room Info else Create New Room
 * 2. Get Messages
 * 3. Send Message
 * 4. Receive Message
 */

export default function MomoChatScreen(props) {

    const { params: { type, transId, tranId, roomId } } = props

    const friendId = formatPhone(get(props, 'params.friendId', ''))
    const friendName = get(props, 'params.friendName', '')

    const isMounted = useIsMountedRef();
    const isInitialized = useIsInitialized()

    const [roomInfo, setRoomInfo] = useState({ id: roomId })
    const [messages, _setMessages] = useState([]);
    const messagesRef = useRef(messages);
    const setMessages = data => {
        messagesRef.current = data
        _setMessages(data)
    }
    
    const getRoomInfo = async () => {
        try {
            const roomInfoResult: any = await ChatService.getRoomInfo({ type, transId, tranId, roomId, friendId });
            console.log('getRoomInfo.result', roomInfoResult);
            isMounted.current && setRoomInfo(roomInfoResult)
        } catch (error) {
            console.log('getRoomInfo.error', error);
        }
    }

    const getRoomMessages = async () => {
        try {
            const messages: any = await ChatService.getRoomMessages({ roomId });
            console.log('getRoomMessages.messages', messages);
            isMounted.current && setMessages(messages)
        } catch (error) {
            console.log('getRoomMessages.error', error);
        }
    }

    const handleConversationListener = useCallback((msg) => {
        console.log('handleConversationListener.msg', msg)
        const data = JSON.parse(msg.data || '{}') || {};
        const gRPCType = get(msg, 'type', '');
        const { message } = data || {};
        const typeTracking = msg.typeTracking;
        if (message) {
            const { roomId = '' } = message || {};
            if(roomInfo.id === roomId) {
                handleNewMessage({message, gRPCType})
            }
        }
    }, [messages])

    const handleNewMessage = ({ message = {}, gRPCType = '' }) => {

        const newMessage = parseMessageDataFromServer(message, {});
        const duplicateFlag = checkDuplicateMessageFlag(messagesRef.current, newMessage);
        console.log('handleNewMessage.duplicateFlag', duplicateFlag)
        switch (gRPCType) {
            case EnumGRPCType.MESSAGE_TEMP:
                newMessage.status = STATUS_MESSAGE.SENDING;
                setMessages([newMessage, ...messagesRef.current])
                break;
            case EnumGRPCType.MESSAGE_SENT:
                if (duplicateFlag.checkMessageDupRequestId) {
                    let newMessageList = messagesRef.current;
                    let dupRequestIdIndex = findIndex(newMessageList, {'requestId': newMessage.requestId})
                    if(dupRequestIdIndex != -1){
                        newMessageList[dupRequestIdIndex] = newMessage;
                    }
                    setMessages(newMessageList)
                } else {
                    setMessages([newMessage, ...messagesRef.current])
                }
                break;
            case EnumGRPCType.NEW_MESSAGE: 
                if (!duplicateFlag.checkMessageDup) {
                    let typeMessage = get(message, 'parts.payload.type', '');
                    if (typeMessage === 'IMAGE') typeMessage = 'photo';
                    const senderId = get(newMessage, 'user._id', '') || get(newMessage, 'customData._id', '');
                    if (!duplicateFlag.checkMessageDupRequestId) {
                        console.log('handleNewMessage.messages', messagesRef.current)
                        const listNewMessages: any = [newMessage, ...messagesRef.current].sort(
                            (a, b) => parseInt(b._id, 10) - parseInt(a._id, 10)
                        );
                        setMessages(listNewMessages);
                    }
                }
                break;
            case EnumGRPCType.UPDATE_MESSAGE:
                
                break;
            default:
                break;
        }
    }

    useEffect(() => {
        if (isInitialized) {
            console.log('isInitialized', ChatService.userProfile)
            getRoomInfo().then(result => {
                getRoomMessages()
            })
            const conversationListener = MaxApi.listenChat(handleConversationListener)
            return () => conversationListener.remove()
        }
    }, [isInitialized])

    const onSend = (msgs: IMessage[]) => {
        const text = get(msgs, '[0].text', '');
        if (text && typeof text.trim === 'function' && text.trim() !== '') {
            const requestId = generateRequestId();
            const customData = get(msgs, '[0].user', '');
            const userName = get(msgs, '[0].user.name', '');

            ChatService.sendTextMessage({
                requestId,
                roomId: roomInfo.id,
                userName,
                customData,
                text,
                senderId: ChatService.userProfile.phoneNumber,
            })
        }
    }

    return isInitialized ? (
        <GiftedChat
            messages={messages}
            onSend={msgs => onSend(msgs)}
            user={{
                _id: ChatService.userProfile.phoneNumber,
                name: ChatService.userProfile.userName,
                avatar: ChatService.userProfile.userName,
            }}
        />
    ) : <SafeAreaView style={styles.container}>
        <ActivityIndicator size={'large'} />
    </SafeAreaView>
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})