import React, { Component } from 'react';
import { Dimensions, StyleSheet, Pressable, Text} from 'react-native';
import { PanGestureHandler, State } from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';

const AnimatedTouchable = Animated.createAnimatedComponent(Pressable);

const BOX_SIZE = 60;
const { width } = Dimensions.get('window');

const {
    set,
    neq,
    cond,
    eq,
    add,
    multiply,
    lessThan,
    spring,
    block,
    startClock,
    stopClock,
    clockRunning,
    sub,
    defined,
    Value,
    Clock,
    event,
} = Animated;

interface IChatHeadProps {
  onPress: () => void;
}

export default class ChatHead extends Component {

    _onGestureEvent
    _transX
    _transY

    constructor(props: IChatHeadProps) {
        super(props);
    
        const TOSS_SEC = 0.2;
    
        const dragX = new Value(0);
        const dragY = new Value(0);
    
        const gestureState = new Value(-1);
        const dragVX = new Value(0);
    
        this._onGestureEvent = event([
          {
            nativeEvent: {
              translationX: dragX,
              velocityX: dragVX,
              state: gestureState,
              translationY: dragY,
            },
          },
        ]);


        const transX = new Value(0);
        const transY = new Value(0);
        const clock = new Clock();
        const prevDragX = new Value(0);
        const prevDragY = new Value(0);
        const snapPoint = cond(
          lessThan(add(transX, multiply(TOSS_SEC, dragVX)), 0), 
          -(width / 2 - BOX_SIZE/2), (width / 2 - BOX_SIZE/2)
        );

        const config = {
          damping: 12,
          mass: 1,
          stiffness: 150,
          overshootClamping: false,
          restSpeedThreshold: 0.001,
          restDisplacementThreshold: 0.001,
          toValue: snapPoint,
        };
    
        const state = {
          finished: new Value(0),
          velocity: dragVX,
          position: new Value(0),
          time: new Value(0),
        };
    
        this._transX = cond(
          eq(gestureState, State.ACTIVE),
          [
            stopClock(clock),
            set(transX, add(transX, sub(dragX, prevDragX))),
            set(prevDragX, dragX),
            transX,
          ],
          cond(neq(gestureState, -1), [
            set(prevDragX, 0),
            set(
              transX,
              cond(
                defined(transX),
                [
                  cond(clockRunning(clock), 0, [
                    set(state.finished, 0),
                    set(state.velocity, dragVX),
                    set(state.position, transX),
                    startClock(clock),
                  ]),
                  spring(clock, state, config),
                  cond(state.finished, stopClock(clock)),
                  state.position,
                ],
                0
              )
            ),
          ])
        );
    
        this._transY = block([
          cond(
            eq(gestureState, State.ACTIVE),
            [
              set(transY, add(transY, sub(dragY, prevDragY))),
              set(prevDragY, dragY),
            ],
            set(prevDragY, 0)
          ),
          transY,
        ]);
    }

    render() {
        const {onPress} = this.props
        return (
            <PanGestureHandler
                maxPointers={1}
                minDist={10}
                onGestureEvent={this._onGestureEvent}
                onHandlerStateChange={this._onGestureEvent}>
                <AnimatedTouchable onPress={() => onPress && onPress()} style={[ styles.box, { transform: [ { translateX: this._transX },  { translateY: this._transY }]}]}>
                    <Text style={styles.title}>Chat</Text>
                </AnimatedTouchable>
            </PanGestureHandler>
        )
    }
}

const styles = StyleSheet.create({
    box: {
      position: 'absolute',
      width: BOX_SIZE,
      height: BOX_SIZE,
      borderColor: '#F5FCFF',
      borderRadius: BOX_SIZE / 2,
      margin: BOX_SIZE,
      backgroundColor: 'red',
      justifyContent: 'center',
      alignItems: 'center',
    },
    title: {
      color: '#FFFFFF',
      fontWeight: 'bold',
      fontSize: 16
    }
});
  