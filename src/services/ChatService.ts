import ChatConstant from "../constants/ChatConstant";
import MaxApi from "@momo-platform/api";
import { formatPhone } from '../helpers/ChatHelper'
import get from 'lodash/get'
import isArray from "lodash/isArray";
import { AttachmentMessageParts, Message, MessageText, PART_TYPE } from '../types'
import { parseMessageDataFromServer } from "./ChatFunctions";

class ChatService {

    userProfile: {
        phoneNumber: string,
        userName: string,
        avatar: string
    }

    endPointAvatar: string

    getProfile() {
        return new Promise((resolve, reject) => {            
            MaxApi.getProfile(result => {
                console.log('ChatService.init.getProfile', result);
                this.userProfile = {
                    phoneNumber: formatPhone(get(result, 'userId', ''), 10) as string,
                    userName: get(result, 'name', ''),
                    avatar: get(result, 'avatar', '')
                }
                resolve(this.userProfile)
            })
        })
    }

    getAvatarEndPoint() {
        return new Promise((resolve, reject) => {
            MaxApi.getAvatarEndPoint(result => {
                this.endPointAvatar = result
                resolve(result)
            })
        })
    }

    getRoomInfo({ type = '', transId = '', tranId = '', roomId = '', friendId = '' }) {

        let source = '';
        let partnerIds = [friendId];
        let trackingId = '';

        if (type === 'masking') {
            source = 'P2P';
            partnerIds = [''];
            trackingId = transId;
        }

        if (tranId) {
            source = 'P2P';
            trackingId = tranId;
        }

        console.log('getRoomInfo.params', {
            roomId, partnerIds, trackingId, source
        });

        return new Promise((resolve, reject) => {
            if (roomId || partnerIds.length >= 1 || trackingId) {
                MaxApi.sendCloudMessage(
                    {
                        path: 'helios/chat-api/v1/room/get/room',
                        body: {
                            roomId,
                            partnerIds,
                            trackingId: trackingId.toString(),
                            source,
                        },
                    },
                    result => {
                        const success = get(result, 'response.success', false)
                        if (success) {
                            const data = get(result, 'response.json.room', {});
                            resolve(data)
                        } else {
                            resolve([])
                        }
                    }
                );
            } else {
                reject(new Error('Missing room Params'))
            }
        })
    }

    getRoomMessages({ roomId = '', beforeId = '', params = {}, limit = 10, action = 1 }) {
        return new Promise((resolve, reject) => {
            MaxApi.sendCloudMessage(
                {
                    path: ChatConstant.ChatApiPath.V1.fetchMessageRoom,
                    body: { roomId, beforeId, limit, action },
                }, result => {
                    const data = get(result, 'response.json.data', [])
                    if(isArray(data)){
                        resolve(data.map(item => {
                            return parseMessageDataFromServer(item, params)
                        }))
                    } else {
                        resolve([])
                    }
                })
        })
    }



    sendTextMessage(message: MessageText): Message {
        const {
            customData, userName, requestId, createdAt, text, roomId, senderId
        } = message;
        const parts: AttachmentMessageParts = {
            partType: PART_TYPE.INLINE, payload: {
                content: text, customData: { ...customData, userName },
            },
        }
        const dateTime = Date.now();
        const _createdAt = createdAt ? createdAt : dateTime;
        MaxApi.chatSendMessageGRPC({
            roomId,
            parts,
            createAt: _createdAt,
            requestId
        });

        return {
            id: _createdAt.toString(),
            createdAt: _createdAt,
            requestId: requestId,
            parts: parts,
            roomId: roomId,
            senderId: senderId
        }
    }

    sendImageMessage() {

    }

    sendReplyMessage() {

    }

    sendStickerMessage() {

    }

    sendReaction() {

    }

    startTyping() {

    }
}

const chatService = new ChatService();

export default chatService;

