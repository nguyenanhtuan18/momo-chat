import MaxApi from '@momo-platform/api';

class Connection {
    
    isConnecting: boolean;
    isConnected: boolean;
    isDisconnected: boolean;

    constructor(){
        this.isConnecting = false;
        this.isConnected = false;
        this.isDisconnected = false;
    }
    
    connect() {
        this.isConnecting = true;
        try {
            MaxApi.chatGRPCConnect(); 
            this.isConnected = true;
        } catch (error) {
            this.isDisconnected = true;
        } finally {
            this.isConnecting = false;  
        }   
    }

    disconnect(){
        this.isConnecting = false;
        this.isConnected = false;
        this.isDisconnected = true;
    }
}

const connection = new Connection();

export default connection;

