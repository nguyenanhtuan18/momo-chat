import { formatPhone } from "../helpers/ChatHelper";
import ChatService from "./ChatService";

export function getAvatarByPhone(phone: string) {
    if (!ChatService.endPointAvatar || ChatService.endPointAvatar?.length === 0) return '';
    return `${ChatService.endPointAvatar}${formatPhone(phone, 11)}.png`;
}

export function parseMessageDataFromServer(payload: any, roomData: any) {
    if (!payload || (payload.hideFrom && payload.hideFrom.includes(ChatService.userProfile.phoneNumber))) {
        return null;
    }
    let data = {
        _id: payload.id,
        createdAt: payload.createdAt,
        requestId: payload.requestId,
        user: {
            _id: formatPhone(payload.senderId, 11),
            name: payload.senderName,
            avatar: getAvatarByPhone(payload.senderId),
        },
        messageStatus: payload.messageStatus || {},
        reaction: payload.reaction || {},
    };
    let parts = payload.parts;
    if (parts && parts.partType && parts.payload) {
        let { customData, type, content, url, base64 } = parts.payload;
        data.customData = customData;
        if (base64) { data.base64 = base64 }
        if (customData && customData?.name) {
            data.user.name = customData?.name || customData?.userName;
        }
        switch (parts.partType) {
            case 'SYSTEM_PIN_MESSAGE':
            case 'INLINE':
                if (type === 'SYSTEM' || type === 'SYSTEM_CTA') {
                    data.system = true;
                    data.text = content;
                    data.type = type;
                    data.url = url;
                    if (roomData && roomData.users && roomData.users.length > 0) {
                        roomData.users.forEach(user => {
                            if (user instanceof Object) {
                                data.text = data.text; 
                            }
                        });
                    }
                } else if (type === 'SYSTEM_LIXI') {
                    const systemLixiObject = customData[ChatService.userProfile.phoneNumber];
                    if (systemLixiObject) {
                        data.system = true;
                        data.isSystemLixi = true;
                        data.quickReplies = {
                            config: systemLixiObject.quickReplies,
                        };
                        data.text = systemLixiObject.text;
                    }
                } else {
                    data.text = content.replace(/<[^>]*>?/gm, '');
                }
                break;
            case 'ATTACHMENT':
                if (type) {
                    data.image = url;
                    data.refId = customData && customData.refId;
                    data.quickReplies = customData && customData.quickReplies;
                    data.customId = payload.customId;
                    switch (type) {
                        case 'IMAGE':
                        case 'TRANSFER':
                            data.text = content;
                            break;
                        case 'REQUEST':
                            data.text = undefined;
                            break;
                        case 'BIRTHDAY':
                            data.customData.messageType = 'BIRTHDAY';
                            data.content = content;
                            break;
                        case 'SEND_GIFT':
                            data.customData.messageType = 'SEND_GIFT';
                            break;
                        case 'REPLY':
                            data.customData.messageType = 'REPLY';
                            data.content = content;
                            break;
                        case 'STICKER':
                            data.image = url;
                            break;
                        default: {
                            break;
                        }
                    }
                }
                break;
            default:
        }
    }
    return data;
}


export function checkDuplicateMessageFlag(listMessage, newMessage) {
    let checkMessageDup = false;
    let checkMessageDupRequestId = false;
    if (listMessage.length > 0) {
        listMessage.forEach(item => {
            if (item._id === newMessage._id) {
                checkMessageDup = true;
            }
            if (
                newMessage.requestId &&
                newMessage.requestId.length > 0 &&
                item.requestId === newMessage.requestId
            ) {
                checkMessageDupRequestId = true;
            }
        });
    }
    return {
        checkMessageDup,
        checkMessageDupRequestId,
    };
}