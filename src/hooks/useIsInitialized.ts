import { useEffect, useState } from "react";
import ChatService from "../services/ChatService"
import ConnectionService from "../services/ConnectionService";

export const useIsInitialized = () => {

    const [isInitialized, setIsInitialized] = useState(false)

    const init = async () => {
        //1.Connect to Chat
        ConnectionService.connect()
        //2. Get Profile
        await ChatService.getProfile()
        //3. Get Avatar EndPoint
        await ChatService.getAvatarEndPoint()

        setIsInitialized(true)
    }

    useEffect(() => {
        init()
    }, [])

    return isInitialized
}