import { LocalizedStrings } from '@momo-kits/core';
import { vi } from './lang/vi';
import { en } from './lang/en';

const ChatLocalization = new LocalizedStrings({
    vi,
    en,
}) as typeof vi;

export default ChatLocalization;
