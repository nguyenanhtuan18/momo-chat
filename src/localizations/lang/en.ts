export const en = {
    ago: 'ago',
    activeNow: 'Active now',
    titlePermission: 'Allow MoMo to access your contacts to see your friends on MoMo',
    accessContact: 'Contact access',
    inputFriendNumber: 'Search ',
    pendingMessage: 'Message requests',
    newMessage: 'New message',
    copyMessage: 'Copy',
    reportMessage: 'Report',
    deleteSuccess: 'Deleted this conversation!',
    spamList: 'Spam list',
    titleReportRoom: 'Are you sure to report this user?',
    titleDeleteRoom: "You won't be able to recover these messages, are you sure?",
    report: 'Report',
    delete: 'Delete',
    deleteMod: 'Xoá vai trò',
    follow: 'Follow',
    unfollow: 'Unfollow',
    replyMessage: 'Reply',
    sentAt: 'Sent at',
    friend: 'Friend',
    stranger: 'Stranger',
    image: 'Image',
    transfer: 'Money Transfer',
    requestMoney: 'Money Request',
    replyYourSelf: 'Replying to yourself',
    replying: 'Replying to',
    sayHiMessage: "Hello, I am @. Let's be friends on MoMo!",
    messageBlock1: 'Cannot send messages because you have blocked this person',
    messageBlock2: 'Cannot send messages because this person has blocked you',
    iKnow: 'Understand',
    selectFriend: 'Select friends',
    existedInRoom: 'These selected are already in the room',
    toastAddMore: '@ persons have been added to the room',
    and: 'and',
    others: 'others',
    canceled: 'Cancelled',
    accepted: 'Accepted',
    titleAddFriend: 'Friend request',
    addFriendFromContact: 'Add friends from contact book',
    cancel: 'Cancel',
    all: 'All',
    cantChatYourSelf: 'You cannot chat with yourself',
    friendNotFound: 'Friend not found',
    searchRecent: 'Recents',
    titlePending: 'Pending message requests',
    warningAddGroup: 'Cannot add more than 20 people at a time',
    notification: 'Alert',
    done: 'Done',
    reject: 'Reject',
    success: 'Successful',
    successWithoutMoMo: 'Paid Without MoMo',
    waiting: 'Awaiting',
    titleTransferSender: 'You transferred',
    titleTransferReceiver: 'You received',
    titleTopupSender: 'You topped up',
    titleTopupReceiver: 'You was topped up',
    giftCard: 'Gift card',
    giftCode: 'Code',
    giftPassWord: 'Password',
    giftMoney: 'Gifting Money',
    lixi: 'Red Packet',
    rejected: 'Rejected',
    friendAccept: 'Accepted',
    requested: 'Request sent',
    toastFriend: 'Đã gửi lời mời kết bạn',
    toastFriendFail: 'Gửi mời kết bạn không thành công',
    addFriend: 'Add friend',
    maximumGroupMember: 'Maximum group member is 20.',
    titleRequestFrined: 'Friend Request',
    lastMessTransfer: '[Money transfer]',
    lastMessRequest: '[Request money]',
    lastMessImage: '[Image]',
    lastMessBirthDay: '[Birthday gift]',
    lastMessTopup: '[Top up]',
    lastMessLuckyMoney: '[Lucky Money]',
    lastMessMonneyBirthday: '[Transfer money for bitthday]',
    lastMessageSticker: '[Sticker]',
    lastMessageTelco: '[Top up]',
    replyTitleTelco: 'Top up',
    lastMessMoneyGift: '[Transfer money gift]',
    lastMessCallback: '[Let me out]',
    hideRoom: 'Hide conversation?',
    hide: 'Hide',
    descHideRoom:
        "You can still find this conversation by searching through the person's phone number. To unhide, send a message to the conversation.",
    titleMuteRoom: 'Are you sure you want to turn on notifications for this conversation?',
    titleUnMuteRoom: 'Are you sure you want to turn off notifications for this conversation?',
    muteRoom: 'UnMute',
    unMuteRoom: 'Mute',
    titleEmptyMessage: "You don't have any conversation yet",
    descEmptyMessage: 'Say Hi and start chatting with friends on MoMo',
    descEmptyMessagePage:
        'Hãy theo dõi với Trang chính thức xem, biết đâu được người nổi tiếng trả lời đấy',
    titleLiveGroup: 'Leave group',
    descLiveGroup: 'Confirmed to leave the group',
    liveGroup: 'Leaving the group',
    editGroupName: 'Edit group name',
    editGroupDesc: 'Edit description',
    save: 'save',
    viewAll: 'View all',
    titleDeleteUser: 'Are you sure you want to remove @ from the chat?',
    confirm: 'Confirm',
    member: 'MEMBER ',
    viewProfile: 'View Profile',
    chat: 'Chat',
    deleteFromChat: 'Delete from chat',
    warningDeveloper: 'This feature is under development !',
    inputMessage: 'Enter your message',
    balance: 'Balance: ',
    titleInputMoney: 'Enter amount',
    enterMessage: 'Enter message',
    btnTitleRequest: 'Request Money',
    titleMessage: 'Message',
    wouldBeFriend: '{name} want to connect and become friend with you on MoMo',
    waitingAccept: 'Waiting for {name} to accept your friend request',
    sayHiTo: 'This person is not in your friendlist',
    sentFriendRequest: 'Request sent',
    warningFriend: 'Be cautious when messaging and transferring with strangers',
    titleFriendAccept: 'Accept',
    friendInMomo: 'You are friends on MoMo',
    isExist: 'These contacts are already in the group chat.',
    titleMember: 'Group members',
    you: 'You',
    titleCreateGroup: 'Select friends',
    textSuggest1: 'Wealth, prosperity and happiness',
    textSuggest2: 'Prosperity and fortune',
    textSuggest3: 'Success and joy',
    textSuggest4: 'Abundance and happiness',
    addToGroup: 'Add to group',
    addTheme: 'Add gift card',
    sendTo: 'Send to',
    phoneNumber: 'Phone number',
    alertRequestGroup: 'Request group money transfer',
    alertTransferGroup: 'group money transfer sucessfully',
    amount: 'Amount',
    titleNetWorkErr: 'Úi, mất kết nối rồi!',
    descNetWorkErr:
        'Có thể do mạng yếu hoặc chưa kết nối internet. Bạn hãy kiểm tra và thử lại nhé.',
    refresh: 'Thử lại',
    descAddFriendFromContact:
        'Cho phép MoMo truy cập danh bạ để tiện trò chuyện với bạn bè hơn nhé',
    titleButtonAskContactPermission: 'Cho phép',
    titleListFriend: 'FRIEND LIST',
    textMinute: 'minute',
    textMinutePlurals: 'minutes',
    textHour: 'hour',
    textHourPlurals: 'hours',
    textDay: 'day',
    textDayPlurals: 'days',
    textMonth: 'month',
    textMonthPlurals: 'months',
    textYear: 'year',
    textYearPlurals: 'years',
    online: 'Online',
    connectSucces: 'Kết nối thành công',
    connectFail: 'Kết nối bị gián đoạn, chúng tôi đang kết nối lại ${param}',
    birthdayOf: 'Hôm nay là sinh nhật của ${user}',
    giftBirthDay: 'Hãy gửi tặng món quà thật dễ thương nhé',
    tabChat: 'Messages',
    tabFeed: 'MoMo Feed',
    feedSeeMore: 'See More',
    errorImage: 'Lỗi kết nối vui lòng thử lại',
    feedEmpty: 'Bạn chưa có bài viết nào',
    countLikes: ' lượt thích',
    sameFriends: ' bạn chung',
    friends: 'Có {number} bạn bè',
    strangerPeople: 'Chưa kết bạn',
    reviewRequest: 'Đang xem xét',
    blockPeople: 'Đang chặn',
    numberOfLikes: 'Interacted',
    maxUser: 'You selected enough {0} persons',
    titleMoney: 'Total money',
    titleMessageRM: 'Message ({0}/160)',
    titleListUser: 'REQUESTED LIST',
    titleButton: 'Send request',
    msgErrorNotMe: 'There must be a payer in the request',
    msgErrorMinValue: 'Minimum amount is {0}đ',
    msgErrorMaxValue: 'Maximum amount is {0}đ',
    commentDefault: 'Safe, free money transfer 24/7 via MoMo',
    requestFail: 'Request failed to send',
    me: 'Me',
    minRequestMoneyItem: 'Minimum amount is {0}đ',
    maxRequestMoneyItem: 'Maximum amount is {0}đ',
    blocked: 'You have blocked this person. Please delete to continue.',
    isBlocked: 'This person has blocked you. Please delete to continue',
    cancelShort: 'Cancel',
    update: 'Update',
    _member: 'members',
    exchangeError: 'Exchange error',
    close: 'Close',
    uppercaseCancel: 'HUỶ',
    uppercaseDeleteAll: 'XOÁ TẤT CẢ',
    deleteAllAlertTitle: 'Xoá lịch sử tìm kiếm?',
    deleteAllAlertBody:
        'Thao tác này sẽ gỡ tất cả các mục trong danh sách tìm kiếm gần đây trên thiết bị này, tuy nhiên không ảnh hưởng đến các hoạt động trò chuyện khác của bạn và vẫn có thể xuất hiện trong những lần tìm kiếm sau này.',
    searchHistoryTips: 'Các thay đổi này sẽ chỉ áp dụng cho danh sách tìm kiếm gần đây của bạn.',
    uppercaseRecentSearch: 'TÌM KIẾM GẦN ĐÂY',
    deleteAll: 'Xoá tất cả',
    uppercaseAllContact: 'TẤT CẢ DANH BẠ',
    editRecentSearch: 'Chỉnh sửa tìm kiếm gần đây',
    edit: 'Chỉnh sửa',
    uppercaseIsActive: 'ĐANG HOẠT ĐỘNG',
    searchTips:
        'Mẹo! MoMo có thể tìm kiếm người lạ giúp bạn. Hãy nhập đủ thông tin SĐT của họ nhé!',
    detailMessageChat: 'View detail',
    leftGroup: 'Left of the group',
    noOneSeen: 'No one seen',
    searchMoMoUser: 'NGƯỜI DÙNG MOMO',
    searchMoMoGroup: 'NHÓM',
    officialPage: 'Official Page',
    notInTheRoom: 'Bạn không còn là thành viên trong cuộc hội thoại này',
    fromUser: 'From ${name}',
    pinMessage: 'Tin nhắn đã ghim',
    transferMultiValidate: 'Mỗi giao dịch chỉ chuyển cho một người',
    selectAll: 'Chọn tất cả',
    chose: 'Chọn',
    somethingWrong: 'Something went wrong, please try again later',
    paid: 'Trả rồi nha',
    payAll: 'Trả hết',
    complete: 'Hoàn tất',
    transferred: 'Đã chuyển',
    received: 'Đã nhận',
    notReceive: 'Chưa nhận',
    waitingTransfer: 'Chờ chuyển',
    beDenied: 'Bị từ chối',
    denied: 'Đã từ chối',
    paidOutMoMo: 'Đã trả ngoài MoMo',
    notHaveYou: 'Đã gửi trong đó không có bạn',
    handled: 'Đã được xử lý',
    receivedOutMomo: 'Đã nhận ngoài MoMo',
    transferredOutMomo: 'Đã chuyển ngoài MoMo',
    waitingYouConfirm: 'Đang chờ Bạn xác nhận',
    waitingConfirm: 'Đang chờ xác nhận',
    refuseReceivedOutMomo: 'Đã từ chối nhận tiền ngoài MoMo',
    beRefuseReceivedOutMomo: 'Xác nhận chuyển ngoài MoMo đã bị từ chối',
    titleLinkRequestMoney: 'Yêu cầu chuyển tiền bằng link',
    answerMessage: 'Trả lời tin nhắn',
    message: 'Tin nhắn',
    yourFriendWaitingConfirm: 'Người bạn của bạn đang chờ xác nhận',
    yourMoneyWaitingConfirm: 'Số tiền của bạn đang được chờ xác nhận',
    choseTheSender: 'Chọn người chuyển tiền',
    privacy: 'Chat manager',
    configBlockPendingRoom: 'Receive messages from strangers',
    warningNotReceiveMessage: 'does not receive messages from strangers. Make friends now to chat.',
    warningReplyMessage: 'If you reply, you will receive new messages from',
    friendRequest: 'Friend request',
    feedHeaderLacXi: 'MoMo LẮC XÌ',
    feedHeaderMovie: 'MoMo Cinema',
    feedExploreNow: 'Explore Now',
    privacySettingHeader: 'Privacy Setting',
    privacySettingOnlyMe: 'Only me',
    privacySettingFriends: 'My friends',
    privacySettingFriendsOfFriends: 'Friends of acquaintances',
    listPostReaction: 'Reaction list',
    profilePage: 'Profile page',
    hideConversation: 'Hide conversation',
    blockList: 'Block list',
    titleHideConversation: 'Hide conversation',
    desHideConversation:
        'Danh sách các cuộc trò chuyện đã ẩn. Vuốt sang trái để bỏ ẩn nếu cần bạn nhé!',
    unblock: 'Unblock',
    emptyHideConversation: 'Bạn chưa ẩn cuộc hội thoại nào!',
    emptyBlockList: 'Bạn chưa ẩn cuộc hội thoại nào!',
    descUnHideRoom: 'Bạn có muốn bỏ ẩn với cuộc hội thoại này?',
    unHideRoom: 'Bỏ ẩn cuộc hội thoại ?',
    unHideRoomTitle: 'Bỏ ẩn cuộc hội thoại',
    unHide: 'Bỏ ẩn',
    unBlock: 'Bỏ chặn',
    desUnBlock: 'Bạn có chắc chắn muốn bỏ chặn người này',
    messageShareKol: 'Nào mình cùng MoMo để gần với người nổi tiếng hơn nhé!',
    share: 'Share',
    sendRequestMoneySuccess: 'Gửi yêu cầu chuyển tiền thành công',
    transferSuccess: 'Chuyển tiền thành công',
    copied: 'Copied',
    reportSuccess: 'Đã báo cáo thành công',
    messageNotSupport: 'Tin nhắn chưa được hỗ trợ ở phiên bản hiện tại...',
    admin: 'Admin',
    review: 'Xem thử',
    warningNoKYC:
        'This account does not verify by MoMo yet. Please careful when making a transfer with this account.',
    warningBlackList:
        'Account has the scam mark or fakes the other function of kernel/company. Please careful when making a transfer with this account.',
    birthdayPinTitle: 'Hôm nay là sinh nhật của',
    birthdayPinDesc: 'Hãy gửi tặng món quà thật dễ thương nhé',
    oaWelcome: 'Chào mừng bạn đến với Official Account',
    oaStartFollow: 'Nhấn quan tâm ngay để nhận nhiều ưu đãi',
    follower: 'người quan tâm',
    benefit: 'Ưu đãi',
    youFollow: 'Bạn đã quan tâm',
    desToastFollow: 'Đã quan tâm. Từ giờ bạn sẽ nhận được các thông tin mới nhất từ',
    desToastFollowError: 'Có lỗi trong quá trình xử lý. Vui lòng thử lại',
    desToastUnfollow: 'Đã bỏ quan tâm. Từ giờ bạn sẽ không nhận được các thông tin mới nhất từ',
    desToastUnfollowError: 'Có lỗi trong quá trình xử lý. Vui lòng thử lại',
    fail: 'Thất bại',
    minAmountTransfer: 'Số tiền chuyển tối thiểu là 100 đ',
    maxAmountTransfer: 'Số tiền chuyển tối đa là 20.000.000 đ',
    requestMoneyBlock: 'Không thể gửi yêu cầu chuyển tiền do bạn đã chặn người này',
    requestMoneyBlockPassive: 'Không thể gửi yêu cầu chuyển tiền do người này đã chặn bạn',
    createRoomSuccess: 'Create group successfuly',
    pinBirthdayMultiUser: 'và ${number} người khác',
    store: 'Store',
    userNotVerify: 'This user does not have a MoMo account or has an error processing data',
    understand: 'Understand',
    quickTransfer: 'Fast money transfer',
    sendBlueprintGift: 'Send',
    self: 'chính mình',
    quickTransferProgressTips: 'Swipe up to quick transfer',
    onBoardingSticker:
        'Bạn có thể chuyển tiền, gửi nhãn dán, nạp tiền điện thoại để bắt đầu trò chuyện',
    onBoardingBlank: 'Chuyển tiền/ đòi tiền miễn phí',
    sendMessageBirthday: 'Hãy gửi lời chúc mừng sinh nhật đến',
    back: 'Back',
    errorTransferFailed: 'Chuyển tiền thất bại',
    defaultErrorTransfer:
        'Đã có lỗi xảy ra trong lúc chuyển tiền, bạn hãy vào lịch sử giao dịch để xem thêm chi tiết',
    onBoardingTransfer: 'Quẹt trái để chuyển tiền nhanh',
    onBoardingTransfer2010: 'Vuốt sang trái để chuyển tiền mừng 20/10',
    errorDetailMessage: 'Tin nhắn không hợp lệ',
    onBoardingP2P1: 'Xem chi tiết giao dịch của bạn',
    onBoardingP2P2: 'Chuyển tiền/nhắc bạn bè chuyển tiền nhanh nè',
    deletedMessageText: 'Tin nhắn không khả dụng',
    textCoverImage: 'Hình ảnh gửi trong @ đã được che đi',
    textShowOriginalImage: 'Nhấn để xem hình ảnh gốc',
    textPendingMessage: 'tin nhắn chờ',
    textSpamMessage: 'tin nhắn spam',
    receiveMessageSetting: 'Setting',
    friendOnMoMo: 'Bạn bè trên MoMo',
    cannotChangeOption: 'Bạn không thể thay đổi tuỳ chọn này',
    phoneInContact: 'Số điện thoại lưu trong danh bạ',
    friendOfFriendOnMoMo: 'Bạn bè của bạn bè trên MoMo',
    transferFromStranger: 'Người lạ chuyển tiền dưới 1000đ',
    kycStranger: 'Người lạ ĐÃ xác thực tài khoản',
    notKycStranger: 'Người lạ CHƯA xác thực tài khoản',
    resetSetting: 'Khôi phục cài đặt mặc định',
    whoCanTextMe: 'Who can text me?',
    whatHappenIfIChangeConfig: 'Chuyện gì xảy ra nếu thay đổi cài đặt nhận tin?',
    happenWhenChangeConfig:
        'Tin nhắn gửi từ những người bạn cho phép sẽ hiển thị thông báo và có thể xem tại màn hình chính của Bạn bè. Các tin nhắn khác sẽ nằm ở mục Tin nhắn đang chờ và không hiển thị thông báo.',
    noteWhenChangeConfig:
        'Lưu ý: Các tin nhắn chứa giao dịch (chuyển tiền, nạp tiền điện thoại,... luôn hiển thị ở màn hình chính của bạn.',
    reset: 'Reset',
    hintConfigTransfer:
        'Nếu không cho phép, bạn vẫn nhận được tiền chuyển và tin nhắn sẽ ở trong Tin nhắn đang chờ',
    hintConfigConfirm: 'Tài khoản đã được MoMo định danh',
    hintConfigUnconfirm: 'Tài khoản chưa được MoMo định danh',
    saveSuccess: 'Save',
    more: 'Option',
    pinConversation: 'Pin conversation',
    unpinConversation: 'Unpin conversation',
    limitPinTitle: 'Bạn chỉ có thể ghim tối đa 5 cuộc hội thoại',
    limitPinContent: 'Bỏ ghim cuộc hội thoại khác để ghim thêm bạn nha',
    pinSuccess: 'Pin success',
    option: 'Option',
    newGroup: 'New group',
    createGroup: 'Create group',
    groupIsCreatedBy: 'Group is created by',
    changeGroupName: 'Change group name',
    notInYourContact: 'Add this person to your friendlist',
    acceptFriendRequest: 'Accept friend request?',
    enterGroupName: 'Enter group name',
    wouldBeYourFriend: ' would be your friend on MoMo',
    pinThisMessage: 'Pin this message',
    unpin: 'Unpin',
    swipeToDelete: 'Swipe to delete',
    sticker: 'Sticker',
    thisMessageIsPinned: 'This message is pinned',
    limitAddFriendTitle: 'Gửi lời mời kết bạn thất bại',
    limitAddFriendContent:
        'Bạn chỉ có thể gửi lời mời kết bạn đến 10 bạn mỗi ngày. Mai hãy quay lại nha',
    limitMessageTitle: 'Gửi tin nhắn thất bại',
    limitMessageContent: 'Uiii, bạn nhắn chậm lại xíu để bạn bè có thể đọc kịp với nha',
    spamMessageTitle: 'Tin nhắn không phù hợp',
    spamMessageContent: 'Tin nhắn chứa nội dung không phù hợp, bạn vui lòng xem lại giúp MoMo nhé!',
    blockedMessageTitle: 'Gửi tin nhắn thất bại',
    blockedMessageContent: 'Tin nhắn không thể gửi do bạn đã bị chặn',
    viewMember: 'Xem thành viên',
    approveNewMember: 'Phê duyệt thành viên mới',
    outConversation: 'Rời cuộc trò chuyện',
    detailPinConversation: 'Ghim trò chuyện',
    detailHideConversation: 'Ẩn trò chuyện',
    other: 'KHÁC',
    addMod: 'Bổ nhiệm làm phó nhóm',
    browse: 'DUYỆT THÀNH VIÊN',
    browseContent:
        'Khi bật chế độ này, thành viên phải được trưởng nhóm và phó nhóm duyệt mới có thể tham gia nhóm.',
    countBrowers: 'YÊU CẦU THAM GIA NHÓM',
    emptyBrowers: 'Chưa có yêu cầu tham gia nào!',
    addModsTitle: 'Bổ nhiệm @ làm phó nhóm?',
    removeModsTitle: 'Xoá vai trò @ làm phó nhóm?',
    removeModsContent: '@ không thể duyệt thành viên và thay đổi cài đặt của nhóm',
    addModsConfirm: 'Bổ nhiệm',
    addAdminConfirm: 'Trao quyền',
    addModsCancel: 'Trao quyền',
    addModsContent: '@ có thể duyệt thành viên và thay đổi cài đặt của nhóm',
    addAdminTitle: 'Trao quyền trưởng nhóm cho @?',
    addAdminContent:
        '• @ sẽ trở thành trưởng nhóm và có mọi quyền quản lý nhóm\n• Bạn không có quyền quản lý nhóm nhưng vẫn là thành viên của nhóm\n• Hành động này không thể hoàn tác',
    switchAdmin: 'Trao quyền trưởng nhóm',
    mod: 'Mod',
    settingGroup: 'Setting group',
    changeMessage: 'Change message',
    warningFullPinMessage: 'Tin nhắn đã ghim đầy, chọn 1 tin để thay đổi',
    unpinThisMessage: 'Unpin this message',
    see: 'Xem',
    toastUpdateInfo: 'Cập nhật thông tin thành công',
    kyc: 'Đã xác thực',
    nonKyc: 'Chưa xác thực',
    approve: 'Approve',
    resend: 'Resend',
    sent: 'Sent',
    failure: 'Failure',
    check: 'Kiểm tra',
    sentMoney: 'Bạn đã nạp tiền điện thoại cho',
    receiveMoney: 'đã nạp tiền điện thoại cho bạn',
    faq: 'Trung tâm trợ giúp',
    tooltipFaq: 'Nếu có thắc mắc, bạn có thể truy cập "Trung tâm trợ giúp"',
    addMemberWithLink: 'Mời vào nhóm qua liên kết',
    descAddMemberWithLink: 'Bất kỳ ai đều có thể dùng liên kết này để tham gia nhóm',
    inviteMemberWithLink: 'Mời bạn qua link',
    changeLink: 'Thay đổi link',
    shareLink: 'Chia sẻ',
    confirmChangeLink:
        'Liên kết cũ sẽ bị vô hiệu hoá, không ai có thể sử dụng để tham gia nhóm nữa. ',
    notPermissionChangeLink:
        'Bạn không có quyền đặt lại liên kết. Hãy liên hệ trưởng nhóm hoặc phó nhóm để đặt lại liên kết này nhé!',
    resetLink: 'Đặt lại liên kết ?',
    copyLinkSuccess: 'Sao chép liên kết thành công. ',
    greetingCard: 'Greeting card',
    open: 'Open',
    greetingCardOpened: 'Opened',
    opened: 'Opened',
    expired: 'Expired',
    tooltipCopyLink: 'Nhấn vào đây để sao chép link.',
    titleListAvatarGroup: 'THÀNH VIÊN',
    otherMember: 'người khác',
    isMember: 'là thành viên',
    joinGroup: 'Tham gia nhóm',
    onRequestJoinGroup: 'Đã yêu cầu tham gia',
    informationGroup: 'Thông tin nhóm',
    understood: 'Đã hiểu',
    requestSuccess: 'Bạn chờ xíu nha, trưởng nhóm sẽ sớm "kết nạp" bạn vào nhóm thôi nè!',
    oh: 'Opps',
    titleRequestPending: 'Chờ phê duyệt',
    shortLinkExpired: 'Liên kết hết hạn!',
    descriptionLinkExpired: 'Liên kết đã bị xoá, nhờ bạn bè gửi liên kết mới để tham gia nhóm nha!',
    questionTakeScreenshot: 'Take screenshot?',
    questionWantToSentScreenshot: 'Do you want to share this image?',
    letsMoMoHelpYou: 'Let MoMo help you.',
    shareThisImage: 'Share this screenshot',
    phatLixi: 'Phát lì xì',
    titleQuantityLuckyMoney: 'Số lượng bao lì xì',
    descQuantityLuckyMoney: 'Trong nhóm hiện có ${number} thành viên',
    setPassword: 'Đặt mật khẩu',
    descSetPassword: 'Mật khẩu bạn tạo hoặc là một dãy số có sẵn',
    copy: 'Sao chép',
    note: 'Lời nhắn',
    preview: 'Xem trước',
    completed: 'Hoàn thành',
    copyPasswordSuccess: 'Đã sao chép mật khẩu',
    setPasswordLixi: 'Đặt mật khẩu',
    suggestLuckyMoney1: 'Cảm ơn bạn! ',
    suggestLuckyMoney2: 'Chúc mừng nhé! ',
    suggestLuckyMoney3: 'Chúc mừng chú nhé! ',
    random: 'Chia ngẫu nhiên',
    even: 'Chia đều',
    activeLink: 'Kích hoạt link',
    descActiveLink:
        'Để mời người khác vào nhóm bằng link, hãy nhờ trưởng nhóm hoặc phó nhóm tạo link tham gia.',
    giphy: 'Giphy',
    searchGiphy: 'Tìm kiếm trên GIPHY',
    messageValidatePassword: 'Password must not contain spaces',
    rightPlaceYouTake: 'Right position you take screenshot',
    enterPassword: 'Enter password',
    warningChangeLink: 'Chỉ trưởng nhóm và phó nhóm có thể thay đổi tuỳ chọn này',
    offInviteLink: 'Tắt liên kết mời vào nhóm',
    descriptionOffInviteLink: 'Liên kết mời vào nhóm sẽ bị vô hiệu hoá.',
    wrongPassword: 'Password is incorrect',
    openedLuckyMoney: '{user} đã mở lì xì',
    detailMoneyPool: 'Xem chi tiết quỹ',
    seeMore: 'More',
    fundIsCreatedBy: 'Quỹ tạo bởi',
    errorCode: 'Code',
    titleGreetingCard: 'Gửi thiệp mừng',
    titleVoucher: 'Gửi quà tặng',
    errorRequestTransferFailed: 'Yêu cầu chuyển tiền thất bại',
    defaultErrorRequestTransfer: 'Đã có lỗi xảy ra trong lúc yêu cầu chuyển tiền',
    transactionHistory: 'Transaction report',
    transactionEmpty: 'No transaction yet!',
    statistic: 'Statistic',
    sendAmount: 'Total amount of transferred money',
    receiveAmount: 'Total amount of received money',
    sendTimes: 'Number of times you transfer money',
    receiveTimes: 'Number of times you receive money',
    maxSendAmount: "Highest amount of money you've transferred",
    minSendAmount: "Lowest amount of money you've transferred",
    w2bPromotionMessage:
        'MoMo is FREE to transfer with 30 transactions, up to 10 million/month to 45 domestic banks',
    statusWaiting: 'Waiting',
    statusProcessing: 'Processing',
    statusRefuse: 'Refuse',
    statusDonated: 'Donated',
    statusExpired: 'Expired',
    notSuccess: 'Something wrong!',
    processing: 'Processing',
    groupUpdated: 'Group is updated',
};
