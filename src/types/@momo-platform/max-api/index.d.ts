declare module '@momo-platform/api' {
    import { ImageStyle, StyleProp } from 'react-native';

    export type IMomoProfileModel = {
        userId: string;
        name: string;
        identify: string;
        avatar: string;
        userType: number;
    };

    type IMoMoResponse = {
        status: boolean;
        response: {
            cmdId: string;
            success: boolean;
            data: any;
            errorCode?: number;
        };
    };

    type IMoMoListenEvent = 'onFocusApp' | 'onBlurApp';

    export default class MaxApi {

        static chatGRPCConnect(): void

        static getProfile(callback: (profile: IMomoProfileModel) => void): void;

        static getAvatarEndPoint(callback: (endpoint: string) => void): void;

        static showToast({
            description,
            duration,
            type,
            icon,
            iconStyle,
        }: {
            description: string;
            duration: number;
            type?: 'success' | 'failure';
            icon?: string;
            iconStyle?: StyleProp<ImageStyle>;
        }): void;

        static startFeatureCode(
            miniAppName: string,
            options?: Record<string, unknown>,
            callbackDismiss?: (data: unknown) => void
        ): void;

        static dismiss(dataCallback?: unknown): void;

        static sendMessage(
            {
                isNewPath,
                subDomain,
                path,
                options,
            }: {
                isNewPath?: boolean;
                subDomain?: string;
                path?: string;
                options: Record<string, unknown>;
                method: 'GET' | 'POST' | 'PUT';
                body?: Record<string, unknown>;
            },
            callback: (response: IMoMoResponse) => void
        ): void;

        static setItem(name: string, data: unknown): void;

        static getItem(name: string, callback: (data: unknown) => void): void;

        static trackEvent(
            name: string,
            {
                action,
                stage,
                params,
            }: { action?: string; stage?: string; params?: Record<string, unknown> }
        ): void;

        static listen(
            name: IMoMoListenEvent,
            callback: () => void
        ): {
            remove: () => void;
        };

        static getFeatureById(featureName: string, callback: (result: unknown) => void);

        static openWeb({ url, title }: { url: string; title?: string }): void;

        static currentHost: {
            appId: string;
            serviceCode: string;
            configuration_version: number;
            lastUpdate: number;
        };

        static copyToClipboard(text: string, toastDisplay: string): void;

        static observerProfile(
            callback: (profile: IMomoProfileModel) => void
        ): { remove: () => void } | undefined;

        static throwJSException(
            { error, isFatal }: { error: Error; isFatal?: boolean },
            callback?: () => void
        ): void;

        static errorTrace(parameters: {
            parameters: { flow: string; step: string; errorMessage: string; errorCode: string };
        }): void;

        static countTrace(parameters: { parameters: { flow: string; step: string } }): void;

        static getDeviceInfo(callback: (deviceInfo: any) => void): void;

        static sendCloudMessage

        static realmQuery

        static listenChat
        
        static chatSendMessageGRPC
        
    }
}
