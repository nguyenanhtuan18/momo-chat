export enum MESSAGE_TYPE_GRPC {
    STICKER = 'STICKER',
    IMAGE = 'IMAGE',
    REPLY = 'REPLY',
    text = 'text',
}

export enum GRPC_TYPE {
    RECEIVED = 'RECEIVED',
    TYPING_INDICATOR = 'TYPING_INDICATOR',
    MESSAGE_DELIVERED = 'MESSAGE_DELIVERED',
    REACT_MESSAGE = 'REACT_MESSAGE',
}

export enum PART_TYPE {
    attachment = 'attachment',
    INLINE = 'INLINE',
}

interface AttachmentMessagePayload {
    type?: string;
    content?: string;
    url?: string;
    name?: string;
    size?: number;
    customData?: any;
    base64?: string;
}

export interface AttachmentMessageParts {
    partType: string;
    payload: AttachmentMessagePayload;
}

export enum EnumGRPCType {
    MESSAGE_TEMP = 'MESSAGE_TEMP',
    MESSAGE_SENT = 'MESSAGE_SENT',
    NEW_MESSAGE = 'NEW_MESSAGE',
    UPDATE_MESSAGE = 'UPDATE_MESSAGE',
    UPDATE_BADGE_ICON = 'UPDATE_BADGE_ICON',
    ADD_PIN = 'ADD_PIN',
    REMOVE_PIN = 'REMOVE_PIN',
    READ_CURSOR = 'READ_CURSOR',
    RECEIVED = 'RECEIVED',
    TYPING_INDICATOR = 'TYPING_INDICATOR'
}

export interface MessageSticker {
    sticker: any;
    createdAt: number;
    requestId: any;
    roomId: string;
    senderId: string;
}

export interface Message {
    id: string;
    createdAt: number;
    requestId: any;
    parts: any;
    roomId: string;
    senderId: string;
}

export interface MessageImage {
    url: string;
    requestId: any;
    roomId: string;
    createdAt: number;
    senderId: string;
    base64: string;
}

export interface UploadAvatar {
    status: boolean;
    response: any;
}

export interface MessageReply {
    requestId: any;
    messageReply: any;
    text: string;
    roomId: string;
}

export interface MessageText {
    createdAt?: number;
    requestId: any;
    roomId: string;
    userName: string;
    customData: any;
    text: string;
    senderId: string;
}