
import { PhoneUtil } from '@momo-platform/utils';
import { v4 as uuidv4 } from 'uuid';

function add0ToPhoneNumber(phone) {
    if (!phone.startsWith('0')) {
        phone = '0' + phone;
    }
    return phone;
}

function removeAreaCode(phone) {
    let phoneCp = phone + '';
    phoneCp = phoneCp.replaceAll(' ', '');
    if (phoneCp.startsWith('84')) {
        phoneCp = phoneCp.substring(2);
    } else if (phoneCp.startsWith('84-')) {
        phoneCp = phoneCp.substring(3);
    } else if (phoneCp.startsWith('+84')) {
        phoneCp = phoneCp.substring(3);
    } else if (phoneCp.startsWith('+84-')) {
        phoneCp = phoneCp.substring(4);
    } else if (phoneCp.startsWith('(+84)')) {
        phoneCp = phoneCp.substring(5);
    }
    phoneCp = add0ToPhoneNumber(phoneCp);
    return phoneCp;
}

export function formatPhone(number = '', output = 10) {
    if (number) {
        let phoneNumber = removeAreaCode(number);
        if (phoneNumber?.length !== output) {
            phoneNumber = PhoneUtil.convertPhoneNumerWithMode({
                phone: number,
            });
        }
        return phoneNumber;
    }
}

export function generateRequestId() {
    return uuidv4();
}

